// W pliku potter.js znajduje się tablica postaci z Harry'ego Pottera.
// Pogrupuj je według „domu” do jakiego przynależą (weź pod uwagę tylko osoby oznaczone jako żyjące). Jeśli postać nie ma podanego domu powinna znajdować się pod kluczem "noHouse".
// Nowy obiekt powinien zawierać jedynie klucze z imieniem i typem. Typ powinien być jedną z wartości: student, staff lub none (jeśli postać nie jest ani nauczycielem ani studentem).
// Pamiętaj, aby używać wyłącznie funkcji wbudowanych klasy Array (oprócz funkcji .forEach()), for, foreach i while zdecydowanie odpada.
// Zadanie rozwiąż uzupełniając poniższy kod, tak aby otrzymać poniżej zaprezentowany wynik.
// const resultArray = hogwardArray.reduce((prev, curr) => {
//   // Uzupełnij tutaj
// }, []);
// // Oczekiwany output
// {
//   Gryffindor: [
//     { name: 'Harry Potter', type: 'student' },
//     // ...
//   ],
//   Slytherin: [
//     { name: 'Draco Malfoy', type: 'student' },
//     { name: 'Horace Slughorn', type: 'staff' },
//     { name: 'Dolores Umbridge', type: 'staff' },
//     { name: 'Lucius Malfoy', type: 'none' },
//     { name: 'Gregory Goyle', type: 'student' }
//   ],
//   // ...
//   noHouse: [
//     { name: 'Kingsley Shacklebolt', type: 'none' },
//     // ...
//   ]
// }//

// const hogwardArray = require("potter.js");
const { hogwardArray } = require("./potter.js");

const resultArray = hogwardArray.reduce((prev, curr) => {
  let type;
  if (curr.hogwartsStudent) {
    type = "student";
  } else if (curr.hogwartsStaff) {
    type = "staff";
  } else {
    type = "none";
  }

  const { house, name, alive } = curr;
  if (alive) {
    if (house != "") {
      if (!prev[house]) {
        prev[house] = [];
      }
      prev[house].push({ name, type });
    } else {
      if (!prev["noHouse"]) {
        prev["noHouse"] = [];
      }
      prev["noHouse"].push({ name, type });
    }
  }
  return prev;
}, []);

console.log(resultArray);
