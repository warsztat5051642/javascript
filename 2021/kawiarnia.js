// Stwórz obiekt o nazwie CoffeeShop zawierający następujące zmienne:
// name – string (nazwa sklepu)
// menu – tablica obiektów, każdy z elementów zawiera:
// item (nazwa elementu),
// type (`food` lub `drink`),
// price
// orders – na początku pusta tablica
// i siedem metod:
// addOrder – dodaje nazwę elementu na koniec tablicy orders, jeśli istnieje w menu i zwraca komunikat "Order added!". W przeciwnym razie zwróć "This item is currently unavailable!"
// fulfillOrder – jeśli orders nie jest pustą tablicą zwróć "The {item} is ready!". Jeśli tablica jest pusta zwróć "All orders have been fulfilled!".
// listOrders – zwraca listę przyjetych zamówień, w przeciwnym wypadku pustą tablicę
// dueAmount – zwraca całkowitą należność za realizowane zamówienia.
// cheapestItem – zwraca nazwę najtańszej pozycji w menu.
// drinksOnly – zwraca tylko nazwy pozycji typu drink z menu.
// foodOnly – zwraca tylko nazwy pozycji typu food z menu.
// Zamówienia realizowane są w kolejności FIFO (first-in, first-out)

// Pamiętaj, aby używać wyłącznie funkcji wbudowanych klasy Array (oprócz funkcji forEach()). Iteracje for, foreach i while zdecydowanie odpadają.

// // Przykład działania:

// // Tworzymy sklep o nazwie "Shop1", który zawiera w menu 3 pozycje:
// // [
// //   { item: "cinnamon roll", type: "food", price: 4.99 },
// //   { item: "hot chocolate", type: "drink", price: 2.99 }
// //   { item: "lemon tea", type: "drink", price: 2.50 }
// // ]
// // tablica zamówień jest pusta

// obj.addOrder("espresso"); // "This item is currently unavailable!" (Sklep nie sprzedaje espresso)

// obj.addOrder("hot chocolate"); // "Order added!"
// obj.addOrder("cinnamon roll"); // "Order added!"

// obj.listOrder(); // ["hot chocolate", "cinnamon roll"]

// obj.dueAmount(); // 7.98 (suma cen za hot chocolate i cinnamon roll)

// obj.fulfillOrder(); // "The hot chocolate is ready!"
// obj.fulfillOrder(); // "The cinnamon roll is ready!"
// obj.fulfillOrder(); // "All orders have been fulfilled!" (Wszystkie zamówienia zostały zrealizowane)

// obj.listOrder(); // []

// obj.dueAmount(); // 0.0

// obj.cheapestItem(); // "lemon tea"
// obj.drinksOnly(); // ["hot chocolate", "lemon tea"]
// obj.foodOnly(); // ["cinnamon roll"]

class Coffe {
  constructor(name, menu) {
    this.name = name;
    this.menu = menu;
    this.orders = [];
  }
  // addOrder – dodaje nazwę elementu na koniec tablicy orders, jeśli istnieje w menu i zwraca komunikat "Order added!". W przeciwnym razie zwróć "This item is currently unavailable!"
  addOrder(item) {
    const itemInMenu = this.menu.find((menuItem) => menuItem.item === item);
    if (itemInMenu) {
      this.orders.push(item);
      return "Order added!";
    } else {
      return "This item is currently unavailable!";
    }
  }
  //   }
  //   fulfillOrder – jeśli orders nie jest pustą tablicą zwróć "The {item} is ready!". Jeśli tablica jest pusta zwróć "All orders have been fulfilled!".
  fulfillOrder() {
    if (this.orders.length > 0) {
      const item = this.orders.shift();
      return `The ${item} is ready!`;
    } else {
      return "All orders have been fulfilled";
    }
  }
  // listOrders – zwraca listę przyjetych zamówień, w przeciwnym wypadku pustą tablicę
  listOrder() {
    return this.orders;
  }
  // dueAmount – zwraca całkowitą należność za realizowane zamówienia.
  dueAmount() {
    return this.orders.reduce((acc, i) => {
      const daniezmenu = this.menu.find((menuItem) => menuItem.item === i);
      return acc + daniezmenu.price;
    }, 0);
  }
  // cheapestItem – zwraca nazwę najtańszej pozycji w menu.
  cheapestItem() {
    const cheapest = this.menu.reduce((prev, curr) => {
      return prev.price < curr.price ? prev : curr;
    });
    return cheapest.item;
  }
  // drinksOnly – zwraca tylko nazwy pozycji typu drink z menu.
  drinksOnly() {
    const drinks = this.menu.filter((item) => item.type === "drink");
    return drinks;
  }
  // foodOnly – zwraca tylko nazwy pozycji typu food z menu.
  foodOnly() {
    const food = this.menu.filter((item) => item.type === "food");
    return food;
  }
}

// // Tworzymy sklep o nazwie "Shop1", który zawiera w menu 3 pozycje:
// // [
// //   { item: "cinnamon roll", type: "food", price: 4.99 },
// //   { item: "hot chocolate", type: "drink", price: 2.99 }
// //   { item: "lemon tea", type: "drink", price: 2.50 }
// // ]
// // tablica zamówień jest pusta

const obj = new Coffe("Shop1", [
  { item: "cinnamon roll", type: "food", price: 4.99 },
  { item: "hot chocolate", type: "drink", price: 2.99 },
  { item: "lemon tea", type: "drink", price: 2.5 },
]);

console.log(obj.addOrder("espresso"));
console.log(obj.addOrder("hot chocolate"));
console.log(obj.addOrder("cinnamon roll"));

console.log(obj.listOrder());
console.log(obj.dueAmount());

console.log(obj.fulfillOrder());
console.log(obj.fulfillOrder());
console.log(obj.fulfillOrder());

console.log(obj.listOrder());
console.log(obj.dueAmount());

console.log(obj.cheapestItem());
console.log(obj.drinksOnly());
console.log(obj.foodOnly());
