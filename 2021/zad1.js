// W pliku books.js znajduje się tablica książek. Posegreguj podane książki względem gatunku. Jeśli książka posiada więcej gatunków niż jeden, powinna znaleźć się pod każdym z tych gatunków. Dodatkowo usuń wszystkie pola oprócz tytułu i autora. Użyj wyłącznie funkcji wbudowanych, for zdecydowanie odpada.
// const books = require('./books.js').booksArray;

// const result = books.reduce(
//   // Uzupełnij
// )

// console.dir(result, { depth: null });
// // Oczekiwany output
// {
//   'fantasy': [
//     { title: 'Lord of the Rings', author: 'J.R.R. Tolkien' },
//     { title: 'Harry Potter', author: 'J.K. Rowling' },
//     // ...
//   ],
//   'fiction': [
//     { title: 'The Borthers Karamazov', author: 'Fyodor Dostoyevsky' },
//     // ...
//   ],
//   // ...
// }

const books = require("./books.js").booksArray;

const result = books.reduce(
  (acc, obj) => {
    const { genre, title, author } = obj;
    if (Array.isArray(genre)) {
      genre.forEach((g) => {
        if (!acc[g]) {
          acc[g] = [];
        }
        acc[g].push({ title, author });
      });
      return acc;
    } else {
      if (!acc[genre]) {
        acc[genre] = [];
      }
      acc[genre].push({ title, author });
    }
    return acc;
  },

  {}
);

console.dir(result, { depth: null });
