// Wyobraź sobie, że piszesz program do sprzedaży ciastek w swojej cukierni. Każde ciastko kosztuje 5 zł i każdy klient może kupić tylko jedno ciastko. Każdy klient zapłaci za ciastko monetą lub banknotem o nominale 5, 10 lub 20 zł. Zawsze musisz wydać prawidłową resztę. Napisz program, który sprawdzi, czy będziesz w stanie wypłacić każdemu klientowi resztę.
// Uwaga! Jako nowa cukiernia nie masz wkładu własnego oprócz ciastek. Twój początkowy budżet wynosi 0 zł.

// // Przykład działania
// checkExchange([5, 5, 5, 10, 20]) // => true
// checkExchange([5, 5, 10, 10, 20]) // => false
// checkExchange([10, 10]) // => false
// checkExchange([5, 5, 10]) // => true
_ = require("lodash");

function checkExchange(arr) {
  let piatki = 0;
  let dziesiatki = 0;
  let dwudziestki = 0;
  let czy_wydac = true;

  arr.forEach((i) => {
    if (i === 5) {
      piatki++;
    } else if (i === 10) {
      if (piatki >= 1) {
        dziesiatki++;
        piatki--;
      } else {
        czy_wydac = false;
      }
    } else if (i === 20) {
      if (piatki >= 1 && dziesiatki >= 1) {
        dwudziestki++;
        piatki--;
        dziesiatki--;
      } else if (piatki >= 3) {
        dwudziestki++;
        piatki -= 3;
      } else {
        czy_wydac = false;
      }
    }
  });
  return czy_wydac;
}

console.log(checkExchange([5, 5, 5, 10, 20])); // => true
console.log(checkExchange([5, 5, 10, 10, 20])); // => false
console.log(checkExchange([10, 10])); // => false
console.log(checkExchange([5, 5, 10])); // => true
