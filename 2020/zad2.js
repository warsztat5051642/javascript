// a następnie wykorzystując zawarte w nim dane, zwróć stringa zawierającego wypisane osoby z podziałem na statki, w jakich są załogą oraz manufacturer tego statku. W swoim rozwiązaniu użyj jedynie funkcji wbudowanych, a sam obiekt powinien zostać wygenerowany za pomocą reduce.
// // Oczekiwany output
// Han Solo
// 1. Millenium Falcon, manufacturer: Corellian Engineering
// 2. A-Wing, manufacturer: Kuat Systems Engineering
// 3. Tantive IV, manufacturer: Corellian Engineering Corporation

// Chewbacca
// 1. Millenium Falcon, manufacturer: Corellian Engineering

// Luke Skywalker
// 1. Millenium Falcon, manufacturer: Corellian Engineering
// 2. X-Wing, manufacturer: Incom Corporation

// Darth Vader
// 1. Tie Fighter, manufacturer: Sienar Fleet Systems
// 2. Executor Star Dreadnought, manufacturer: Kuat Drive Yards
// 3. Victory Star Destroyer, manufacturer: Sienar Fleet Systems
// 4. Lambda class T-4a shuttle, manufacturer: Sienar Fleet Systems

// // ...

const ships = require("./ships.js").shipsArray;

const x = ships.reduce((acc, obj) => {
  const { crew, model, manufacturer } = obj;
  crew.forEach((crewmember) => {
    if (!acc[crewmember]) {
      acc[crewmember] = [];
    }
    acc[crewmember].push({ model, manufacturer });
  });
  return acc;
}, {});

// console.log(x);

let result = "";
Object.entries(x).forEach(([crewmember, assignments]) => {
  result += `${crewmember}\n`;
  assignments.forEach((assignment, i) => {
    result += `${i + 1}. ${assignment.model}, manufacturer: ${
      assignment.manufacturer
    }\n`;
  });
  result += "\n";
});

console.log(result);
