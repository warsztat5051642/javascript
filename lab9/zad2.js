// Stwórz funkcję filterObjects wykorzystując bibliotekę lodash, która przyjmuje trzy parametry:
// array - tablicę obiektów,
// criteria - obiekt kryteriów wyszukiwania, gdzie klucze to pola, według których ma być przeprowadzone filtrowanie, a wartości to oczekiwane wartości tych pól,
// partialMatch - opcjonalny parametr typu boolean, który określa, czy dopuszczalne są częściowe dopasowania wartości.
// Funkcja powinna zwracać nową tablicę, zawierającą obiekty, które spełniają wszystkie podane kryteria wyszukiwania. Jeśli partialMatch jest ustawione na true, to wartości pól obiektów mogą być częściowymi dopasowaniami wartości kryteriów.
// // Przykład działania

// const array = [
//   { id: 1, name: 'John', age: 30 },
//   { id: 2, name: 'Alice', age: 25 },
//   { id: 3, name: 'Bob', age: 35 }
// ];

// console.log(filterObjects(array, { name: 'Jo' }, true));
// // Output: [{ id: 1, name: 'John', age: 30 }]

// console.log(filterObjects(array, { age: 3 }, true));
// // Output: []

// console.log(filterObjects(array, { age: 3 }));
// // Output: [{ id: 3, name: 'Bob', age: 35 }]
const _ = require("lodash");

function filterObjects(array, criteria, partialMatch) {
  return _.filter(array, (obj) => {
    return _.every(criteria, (value, key) => {
      if (partialMatch) {
        return _.includes(String(obj.key), String(value));
      } else {
        return obj[key] === value;
      }
    });
  });
}

const array = [
  { id: 1, name: "John", age: 30 },
  { id: 2, name: "Alice", age: 25 },
  { id: 3, name: "Bob", age: 35 },
];

console.log(filterObjects(array, { name: "Jo" }, true));
console.log(filterObjects(array, { age: 3 }, true));
console.log(filterObjects(array, { age: 3 }, false));
